﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace DN.PathFinding
{
    public delegate void AsyncMethodCaller(Point startPosition, Point endPosition);

    public class AStar
    {
        private TileMap _tileMap;

        private List<Vector2> _lastPath; 

        public bool HasEnded = false;
        public bool ImprovedPathFinding = false;
        public bool IgnoreObstacles = false;
        public bool DiagonalMovesAllowed = false;

        byte[,] pointMap;
        BinaryHeap OpenList;

        public AStar() { }

        public AStar(TileMap tileMap)
        {
            _tileMap = tileMap;
        }


        public List<Point> GetPath(Point startCell, Point endCell)
        {
            pointMap = new byte[_tileMap.Width, _tileMap.Height];
            OpenList = new BinaryHeap();

            WayPoint startPoint = new WayPoint(startCell, null, true);
            startPoint.CalculateCost(_tileMap, endCell,0, ImprovedPathFinding);

            OpenList.Add(startPoint);

            while (OpenList.Count != 0)
            {
                WayPoint node = OpenList.Get();
                if (node.PositionX == endCell.X && node.PositionY == endCell.Y)
                {
                    WayPoint nodeCurrent = node;
                    List<Point> points = new List<Point>();

                    while (nodeCurrent != null)
                    {
                        points.Insert(0, new Point(nodeCurrent.PositionX, nodeCurrent.PositionY));
                        nodeCurrent = nodeCurrent.Parent;
                    }
                    return points;
                }

                OpenList.Remove(); 

                Point temp = new Point(node.PositionX, node.PositionY);
                if (DiagonalMovesAllowed)
                {
                        if (CheckPassability(temp.X, temp.Y - 1) && _tileMap[temp.X + 1, temp.Y].Type == CellType.Wall)
                            AddNode(node, 1, -1, false, endCell);
                        if (CheckPassability(temp.X, temp.Y - 1) && _tileMap[temp.X - 1, temp.Y].Type == CellType.Wall)
                            AddNode(node, -1, -1, false, endCell);

                    if (_tileMap.InRange(temp.X, temp.Y + 1))//temporary
                        if (CheckPassability(temp.X + 1, temp.Y) && _tileMap[temp.X, temp.Y + 1].Type == CellType.Wall)
                            AddNode(node, 1, 1, false, endCell);
                    if (_tileMap.InRange(temp.X, temp.Y + 1))//temporary FIX IT!!!
                        if (CheckPassability(temp.X - 1, temp.Y) && _tileMap[temp.X, temp.Y + 1].Type == CellType.Wall)
                            AddNode(node, -1, 1, false, endCell);
                }

                AddNode(node, -1, 0, true, endCell);
                AddNode(node, 0, -1, true, endCell);
                AddNode(node, 1, 0, true, endCell);
                AddNode(node, 0, 1, true, endCell);
            }
            return null;
        }


        private bool CheckPassability(int x, int y)
        {
            if (!_tileMap.InRange(new Point(x, y))) return false;

            return !_tileMap.IsSolid (x, y);
        }
        
        private bool CheckPassability(Point cell)
        {
            return CheckPassability(cell.X, cell.Y);
        }


        private void AddNode(WayPoint node, sbyte offSetX, sbyte offSetY, bool type, Point endCell)
        {
            Point pos = new Point(node.PositionX + offSetX, node.PositionY + offSetY);

            if (!CheckPassability(pos))
                return;
            if (pointMap[pos.X, pos.Y] != 1)
            {
                WayPoint temp = new WayPoint(pos, node,  type);


                int addCost = _tileMap[pos.X, pos.Y].Type == CellType.WalkingCell ? -5000 : 5000;
                temp.CalculateCost(_tileMap, endCell, addCost, ImprovedPathFinding);
                OpenList.Add(temp);
                pointMap[pos.X, pos.Y] = 1;
            }
        }

  

        private Point VectorToPoint(Vector2 vector)
        {
            return new Point((int)vector.x, (int)vector.y);
        }

        private Vector2 PointToVector(Point point)
        {
            return new Vector2(point.X, point.Y);
        }
    }
}