﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class GameObjectsData:MonoBehaviour
{
    public GameObject TurretBase;

    public GameObject Tile;
    public GameObject Wall;

    public GameObject EnemyPrefab;
}
