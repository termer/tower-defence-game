﻿using System.ComponentModel;
using UnityEngine;
using System.Collections;

public class ObjectPool : MonoBehaviour
{

    public static ObjectPool Instance;

    public GameObject prefab;
    public int poolSize = 100;

    private GameObject[] pool;

    void Start()
    {
        pool = new GameObject[poolSize];

        for (int i = 0; i < poolSize; i++)
        {
            pool[i] = (GameObject)Instantiate(prefab, transform.position, Quaternion.identity);
            pool[i].SetActive(false);
        }

        Instance = this;
    }

    public GameObject RetrieveInstance()
    {
        foreach (GameObject go in pool)
        {
            if (!go.activeSelf)
            {
                go.SetActive(true);
                return go;
            }
        }

        return null;
    }
     
    public void DevolveInstance(GameObject go)
    {
        go.SetActive(false);
    }
}
