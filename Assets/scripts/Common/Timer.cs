﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DN
{
    public class Timer
    {
        public bool IsRunning
        {
            get { return _running; }
        }

        public EventHandler OnTick;
        public bool Loop;

        public float Duration;

        private bool _running;
        private float _elapsed;

        public void Run()
        {
            _running = true;
        }

        public void Update(float dt)
        {
            if (!_running)
            {
                return;
            }

            _elapsed += dt;
            if (_elapsed >= Duration)
            {
                if (Loop)
                {
                    _elapsed -= Duration;
                }
                else
                {
                    _elapsed = 0;
                }
                _running = Loop;


                if (OnTick != null)
                {
                    OnTick(this, EventArgs.Empty);
                }
            }
        }
    }
}