﻿using DN;
using UnityEngine;
using System.Collections;

public class AppRoot : MonoBehaviour
{

    public GameObject TurretFakeObject;

    public GameObject TurretLeft;
    public GameObject TurretRight;
    public GameObject TurretUp;
    public GameObject TurretDown;

    private GameObject _currentFakeTurret;

    public GameObject Tile;
    public TileMap TileMap;

    private TurretFabric _turretFabric;
    private ETowerType _currentTowerType;
    private Vector2 _currentTurretDirection = Vector2.right;
    private EnemySpawner _enemySpawner;

	// Use this for initialization
	void Start () 
    {
        _currentFakeTurret = Instantiate(TurretFakeObject) as GameObject;
        _currentFakeTurret.SetActive(false);
        _turretFabric= new TurretFabric(GetComponent<GameObjectsData>());

        TileMap.FillWith(CellType.Free);

        TileMap.LoadFromFile("Level1");
        _enemySpawner = new EnemySpawner(GetComponent<GameObjectsData>(), TileMap);
        _enemySpawner.LoadSpawnData("Level1_Spawn");

        TileMap.GenerateGameObjects(Tile, GetComponent<GameObjectsData>().Wall);
    }
	
	// UpdateMovement is called once per frame
	void Update ()
	{
	    UpdateInput();
	    _enemySpawner.Update();
	}

    private void UpdateInput()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100f, 1 << LayerMask.NameToLayer("Floor")))
        {
            var tile = TileMap.ToTileMap(hit.point);
            var worldTilePosition = TileMap.ToWorld(tile);

            _currentFakeTurret.transform.position = worldTilePosition;
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            _currentFakeTurret.SetActive(true);
            _currentTowerType = ETowerType.SimpleDirectional;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            _currentFakeTurret.SetActive(true);
            _currentTowerType = ETowerType.Freeze;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            _currentTurretDirection = -Vector2.right;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            _currentTurretDirection = Vector2.right;
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            _currentTurretDirection = Vector2.up;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            _currentTurretDirection = -Vector2.up;
        }


        if (Input.GetMouseButtonDown(0))
        {
            if (CreateTurret())
            {
                _currentFakeTurret.SetActive(false);
            }
        }
    }

    private bool CreateTurret()
    {
        var tile = TileMap.ToTileMap(_currentFakeTurret.transform.position);
        
        var worldTilePosition = TileMap.ToWorld(tile);
        
        if (!TileMap.InRange(tile))
        {
            return false;
        }
        if (TileMap.IsWall(tile.X, tile.Y))
        {
            return false;
        }


        var turret = _turretFabric.CreateTurret(_currentTowerType, _currentTurretDirection);
        turret.transform.position = worldTilePosition;
        var t = TileMap[tile];
        t.Type =CellType.Wall;
        TileMap[tile] = t;

        return true;
    }
}
