﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace DN
{
	[StructLayout(LayoutKind.Sequential)]
	public struct Point 
	{
		public int X, Y;
		public Point(int x, int y)
		{
			this.X = x;
			this.Y = y;
		}
		public Point(int value)
		{
			this.X = value;
			this.Y = value;
		}
		public static Point Empty
		{
			get
			{
				return new Point(0);
			}
		}

	    public override string ToString()
	    {
	        return "X " + X + " Y "+ Y;
	    }
	}
	public class Tuple<A, B>
	{
		public A a;
		public B b;

		public Tuple (A a, B b)
		{
			this.a = a;
			this.b = b;
		}
	}
}
