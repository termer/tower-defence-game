﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class GameObjectExtenstion
{
    public static GameObject Create(this GameObject gameObject)
    {
        GameObject newGo = GameObject.Instantiate(gameObject) as GameObject;
        return newGo;
    }
}
