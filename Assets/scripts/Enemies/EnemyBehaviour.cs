﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN;
using DN.PathFinding;
using UnityEngine;
using System.Collections;

public enum ArmorType
{
    None =0,
    Light = 25,
    Medium = 50,
    Heavy = 75,
    Ultra = 95
}

public class EnemyBehaviour : MonoBehaviour
{
    public float Health;
    public float Speed;
    public ArmorType Armor = ArmorType.Heavy;

    public List<Vector3> Path;

    private readonly List<TargetEffect> _effects = new List<TargetEffect>();

    private AStar _aStar;
    private TileMap _tileMap;

    public void Init(TileMap tileMap)
    {
        _tileMap = tileMap;
        _aStar = new AStar(tileMap);
        tileMap.MapChanged += UpdatePath;

        UpdatePath(this, EventArgs.Empty);
        Armor = ArmorType.Heavy;
    }

    private void UpdatePath(object sender, EventArgs eventArgs)
    {
        Path = _aStar.GetPath(CurrentCell, _tileMap.GetFinishCell())
            .Select(p => _tileMap.ToWorld(p))
            .ToList();
    }

    public void AddEffect(TargetEffect effect)
    {
        foreach (TargetEffect targetEffect in _effects.ToList())
        {
            if (targetEffect.IsEnded)
            {
                _effects.Remove(targetEffect);
            }
        }

        var oldEffect = _effects.Find(p => p.GetType() == effect.GetType());
        if (oldEffect != null)
        {
            oldEffect.Join(effect);
        }
        else
        {
            effect.Start(gameObject);
            _effects.Add(effect);
        }
    }

	void Update ()
	{
	    if (Path.Count > 0)
	    {
	        var moveDirection = MoveToNextPoint();
	        CheckPointEnd(moveDirection);
	    }

	    foreach (var effect in _effects)
	    {
	        effect.Update();       
	    }
	}

    private void CheckPointEnd(Vector3 moveDirection)
    {
        Vector3 newDirection = (Path[0] - transform.position).normalized;

        if (Vector3.Dot(moveDirection, newDirection) < 0.01f)
        {
            Path.RemoveAt(0);
        }
    }

    private Vector3 MoveToNextPoint()
    {
        Vector3 moveDirection = (Path[0] - transform.position).normalized;
        Vector3 moveVector = moveDirection*Speed*Time.deltaTime;

        transform.position += moveVector;
        return moveDirection;
    }

    void OnTriggerEnter(Collider collider)
    {
        var projectile = collider.gameObject.GetComponent<Projectile>();

        if (projectile == null)
        {
            return;
        }

        if (projectile.Effects != null)
        {
            projectile.Effects.ForEach(p => p.Collision(gameObject));
        }

        Debug.Log(projectile.Power*GetArmorPercentage());
        Health -= projectile.Power*GetArmorPercentage();

        if (Health <= 0f)
        {
            _tileMap.MapChanged -= UpdatePath;
            Destroy(gameObject);
        }

        Destroy(projectile.gameObject);
}

    private float GetArmorPercentage()
    {
        return 1f - ((float)Armor/100f);
    }


    private Point CurrentCell
    {
        get { return _tileMap.ToTileMap(transform.position); }
    }
}
