﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using DN;
using UnityEngine;


public enum EnemyType
{
    None,
    WeakCube
}

[Serializable]
public class SpawnData
{
    public EnemyType Entity;
    public float Time;

}

public class SpawnWave
{
    public List<SpawnData> Spawns;
    public float Time;
}

public class SpawnCollection
{
    public List<SpawnWave> Waves;

}

public class EnemySpawner
{
    private readonly TileMap _tileMap;
    private readonly EnemyFabric _enemyFabric;

    private readonly Timer _waveStartTimer;

    private SpawnCollection _spawnCollection;

    private SpawnData _currentSpawnedEnemy;


    private float _elapsed;

    private SpawnWave _currentWave;

    public EnemySpawner(GameObjectsData data, TileMap tileMap)
    {
        _tileMap = tileMap;
        _enemyFabric = new EnemyFabric(data, tileMap);
        _waveStartTimer = new Timer();
    }

    public void LoadSpawnData(string name)
    {
        string path = LevelAssetPathHelper.GetRootFolder() + name + ".data";

        XmlSerializer serializer = new XmlSerializer(typeof(SpawnCollection));

        using (StreamReader reader = new StreamReader(path))
        {
            _spawnCollection = (SpawnCollection) serializer.Deserialize(reader);
            Debug.Log(_spawnCollection.Waves.Count);
            Debug.Log(_spawnCollection.Waves[0].Spawns.Count);
            Debug.Log(_spawnCollection.Waves[0].Spawns[0].Time);
            Debug.Log(_spawnCollection.Waves[0].Spawns[0].Entity);
        }
    }

    public void Update()
    {
        _waveStartTimer.Update(Time.deltaTime);

        if (_currentWave == null || _currentWave.Spawns.Count == 0)
        {
            GetNextWave();
        }

        if (!_waveStartTimer.IsRunning)
        {
            if (_currentWave != null)
            {
                startOver: //<----LABEL
                CheckNeedToCreateNewEntity();
                if (_currentSpawnedEnemy != null)
                {
                    _elapsed += Time.deltaTime;
                    if (_elapsed >= _currentSpawnedEnemy.Time)
                    {
                        _elapsed -= _currentSpawnedEnemy.Time;

                        _currentSpawnedEnemy = null;
                        goto startOver; //LABEl
                    }
                }
            }
        }
    }

    private void CheckNeedToCreateNewEntity()
    {
        if (_currentWave.Spawns.Count > 0)
        {
            if (_currentSpawnedEnemy == null)
            {
                CreateNewEntity();
            }
        }
    }

    private void CreateNewEntity()
    {
        _currentSpawnedEnemy = _currentWave.Spawns.First();
        _currentWave.Spawns.RemoveAt(0);
        if (_currentSpawnedEnemy.Entity == EnemyType.None)
        {
            return;
        }

        var go = _enemyFabric.Create(_currentSpawnedEnemy.Entity);
        Debug.Log(go);
        go.transform.position = _tileMap.ToWorld(_tileMap.GetStartCell());
        go.GetComponent<EnemyBehaviour>().Init(_tileMap);
    }


    private void GetNextWave()
    {
        _currentWave = _spawnCollection.Waves.FirstOrDefault();
        if(_currentWave != null)
        {
            _spawnCollection.Waves.RemoveAt(0);

            _waveStartTimer.Duration = _currentWave.Time;
            _waveStartTimer.Run();
        }


    }
}
