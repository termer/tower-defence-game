﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class EnemyFabric
{
    private readonly GameObjectsData _gameObjectsData;
    private readonly TileMap _tileMap;

    public EnemyFabric(GameObjectsData gameObjectsData, TileMap tileMap)
    {
        _gameObjectsData = gameObjectsData;
        _tileMap = tileMap;
    }

    public GameObject Create(EnemyType type)
    {
        GameObject newGo = _gameObjectsData.EnemyPrefab.Create();
        EnemyBehaviour eb = newGo.GetComponent<EnemyBehaviour>();

        switch (type)
        {
            case EnemyType.WeakCube:
                eb.Health = 3f;
                eb.Speed = 1f;
                break;
            default:
                throw new ArgumentOutOfRangeException("type");
        }
        return newGo;
    }
}

