﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    public float MouseBorder = 0.1f;
    public float Sensetivity;
    public float SmoothSpeed;

    public float ZoomSensetivity;
    public float ZoomSmooth;

    public float MinZoom;
    public float MaxZoom;

    public Rect Borders;

    private Camera _camera;

    private Vector3 _targetPos;
    private float _targetZoom;

	// Use this for initialization
	void Start ()
	{
        _camera= GetComponent<Camera>();
	    _targetZoom = _camera.transform.position.y;
	    _targetPos = _camera.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    UpdatePositionControl();

        float zoomDelta = Input.GetAxis("Mouse ScrollWheel") * ZoomSensetivity;
	    _targetZoom = Mathf.Clamp(_targetZoom - zoomDelta, MinZoom, MaxZoom);

        _camera.transform.SetY(Mathf.Lerp(_camera.transform.position.y, _targetZoom, Time.deltaTime * ZoomSmooth));
	}

    private void UpdatePositionControl()
    {
        Vector2 mousePos = _camera.ScreenToViewportPoint(Input.mousePosition);
        mousePos.x = Mathf.Clamp01(mousePos.x);
        mousePos.y = Mathf.Clamp01(mousePos.y);
        Vector3 movement = new Vector3();

        if (mousePos.x < MouseBorder)
        {
            movement.x = mousePos.x - MouseBorder;
        }
        else if (mousePos.x > 1f - MouseBorder)
        {
            movement.x = 1f - mousePos.x - MouseBorder;
            movement.x *= -1;
        }

        if (mousePos.y < MouseBorder)
        {
            movement.z = mousePos.y - MouseBorder;
        }
        else if (mousePos.y > 1f - MouseBorder)
        {
            movement.z = 1f - mousePos.y - MouseBorder;
            movement.z *= -1;
        }

        _targetPos = _targetPos + movement*Sensetivity;

        transform.position = Vector3.Lerp(transform.position, _targetPos, Time.deltaTime*SmoothSpeed);
    }
}
