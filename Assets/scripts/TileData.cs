﻿using UnityEngine;
using System.Collections;

public class TileData : MonoBehaviour
{
    public Light Light;

    public void SetLightActive(bool active)
    {
        Light.gameObject.SetActive(active);
    }

    public void SetColor(Color color)
    {
        renderer.material.color = color;
        Light.color = color;
    }

    
}
