﻿using UnityEngine;

public class SlowdownEffect:TargetEffect
{
    public float SlowdownPercentage = 0.5f;


    private float _startSpeed;
    protected override void InternalStart()
    {
        _startSpeed = EnemyBehaviour.Speed;
        EnemyBehaviour.Speed = Mathf.Clamp(EnemyBehaviour.Speed*(1f - SlowdownPercentage), 0, float.MaxValue);
        Debug.Log("slow down");
    }

    protected override void InternalEnd()
    {
        EnemyBehaviour.Speed = _startSpeed;
        Debug.Log("speed up");
    }

    protected override void InternalUpdate()
    {
        
    }
    public override void Join(TargetEffect effectComponent)
    {
        var other = effectComponent as SlowdownEffect;

        Duration += other.Duration;
        As<SlowdownEffect>().SlowdownPercentage += other.As<SlowdownEffect>().SlowdownPercentage;
    }


}
