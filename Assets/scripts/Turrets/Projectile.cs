﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Projectile:MonoBehaviour
{
    public List<EffectComponent> Effects; 

    public float Speed;

    public float Power;

    public Vector2 Direction;

    public GameObject Creator;

    private void Update()
    {
        transform.position += new Vector3(Direction.x, 0, Direction.y)*Time.deltaTime*Speed;
    }
}
