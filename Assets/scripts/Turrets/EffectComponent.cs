﻿using UnityEngine;

public abstract class EffectComponent
{
    protected TargetEffect TargetEffect { get; private set; }


    protected abstract TargetEffect CreateTargetAction();
    public abstract EffectComponent Clone();



    public void Join(EffectComponent component)
    { 
        if (TargetEffect != null)
        {
            TargetEffect.Join(component.TargetEffect);
        }
    }

    public virtual void Start()
    {
        TargetEffect = CreateTargetAction();
    }

    public virtual void UpdateMovement()
    {

    }

    public bool MustBeAddedToTarget()
    {
        return TargetEffect != null;
    }

    public virtual void Collision(GameObject gameObject)
    {
        Debug.Log("coll");
        if (TargetEffect != null)
        {
            Debug.Log("Add effect 2");
            gameObject.GetComponent<EnemyBehaviour>().AddEffect(TargetEffect);
        }
    }

    public bool CanBeJoined(EffectComponent effectComponent)
    {
        return effectComponent.GetType() == GetType();
    }


    public T As<T>() where T : TargetEffect
    {
        return TargetEffect as T;
    }


}


