﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DN;
using UnityEngine;

public abstract class TargetEffect
{
    protected Timer EndingTimer;
    protected GameObject Target;
    protected EnemyBehaviour EnemyBehaviour;


    public abstract void Join(TargetEffect effectComponent);

    public TargetEffect()
    {
        EndingTimer = new Timer { Duration = 1.0f };
        EndingTimer.OnTick += OnTick;
    }

    public void Start(GameObject target)
    {
        Target = target;

        EnemyBehaviour = Target.GetComponent<EnemyBehaviour>();

        
        EndingTimer.Run();

        InternalStart();
    }

    public void Update()
    {
        EndingTimer.Update(Time.deltaTime);
        InternalUpdate();
    }

    private void OnTick(object sender, EventArgs eventArgs)
    {
        InternalEnd();
    }

    public T As<T>() where T : TargetEffect
    {
        return this as T;
    }

    protected abstract void InternalStart();
    protected abstract void InternalEnd();
    protected abstract void InternalUpdate();


    public float Duration
    {
        get { return EndingTimer.Duration; }
        set { EndingTimer.Duration = value; }
    }

    public bool IsEnded
    {
        get { return !EndingTimer.IsRunning; }
    }
}
