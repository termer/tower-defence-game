﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FreezeComponent:EffectComponent
{
    public override EffectComponent Clone()
    {
        return new FreezeComponent();
    }

    protected override TargetEffect CreateTargetAction()
    {
        return new SlowdownEffect();
    }

}
