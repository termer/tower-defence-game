﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public enum ETowerType
{
    SimpleDirectional,
    Freeze
}

public class TurretFabric
{
    private readonly GameObjectsData _gameObjectsData;

    public TurretFabric(GameObjectsData gameObjectsData)
    {
        _gameObjectsData = gameObjectsData;
    }


    public GameObject CreateTurret(ETowerType type, Vector2 direction)
    {
        var newTower = _gameObjectsData.TurretBase.Create();
        var turret = newTower.GetComponent<DirectionalTurret>();

        turret.Direction = direction;

        switch (type)
        {
            case ETowerType.SimpleDirectional:
                break;
            case ETowerType.Freeze:
                turret.AddBaseEffect(new FreezeComponent());
                break;
            default:
                throw new ArgumentOutOfRangeException("type");
        }


        return newTower;
    }
}
