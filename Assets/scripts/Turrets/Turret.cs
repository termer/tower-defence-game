﻿using System;
using System.Collections.Generic;
using System.Linq;
using DN;
using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour
{
    public float MaxShootCooldown = 3.0f;
    public float MinShootCooldown = 1.0f;

    public float MinPower = 1.0f;
    public float MaxPower = 10.0f;

    public GameObject Projectile;

    private readonly List<EffectComponent> _baseEffects = new List<EffectComponent>(); 
    private List<EffectComponent> _currentEffects = new List<EffectComponent>(); 

    private float _power;
    private Timer _shootTimer;

    private Color _targetColor = Color.white;

	// Use this for initialization
	protected void Start () 
    {
	    _shootTimer = new Timer();
        _shootTimer.OnTick += ShootOnTick;
	    _shootTimer.Duration = MaxShootCooldown;
	    _shootTimer.Loop = true;
        _shootTimer.Run();

    }

    public void AddBaseEffect(EffectComponent effectComponent)
    {
        if (_baseEffects.Find(p => p.GetType() == effectComponent.GetType()) != null)
        {
            Debug.LogError("Already contains component " + effectComponent.GetType().Name);
            return;
        }

        _baseEffects.Add(effectComponent);

        CopyBaseEffectsToCurrent();
    }



    private void ShootOnTick(object sender, EventArgs eventArgs)
    {
        Shoot();
    }

    // UpdateMovement is called once per frame
	protected void Update ()
	{
	    NormalizePower();

	    _shootTimer.Update(Time.deltaTime);

        UpdateTurretColor();

        InternalUpdate();
	}


    protected virtual void InternalUpdate()
    {

    }

    void OnTriggerEnter(Collider collider)
    {
        var projectile = collider.gameObject.GetComponent<Projectile>();

        if (projectile != null)
        {
            if (projectile.Creator != gameObject)
            {
                Destroy(projectile.gameObject);


                Supply(projectile);
            }
        }
    }

    private void NormalizePower()
    {
        _power = Mathf.Max(MinPower, _power);
        _power = Mathf.Min(MaxPower, _power);
    }

    protected virtual void Shoot()
    {
        _power = MinPower;
        _shootTimer.Duration = MaxShootCooldown;

        CopyBaseEffectsToCurrent();
    }

    private void CopyBaseEffectsToCurrent()
    {
        _currentEffects = _baseEffects.Select(p => p.Clone()).ToList();

        foreach (EffectComponent effect in _currentEffects)
        {
            effect.Start();
        }
    }


    public void Supply(Projectile projectile)
    {
        _power += projectile.Power;
        _shootTimer.Duration = Mathf.Lerp(MaxShootCooldown, MinShootCooldown, (_power - MinPower) / MaxPower);

        JoinEffects(projectile);
        NormalizePower();
        UpdateTurretColor();
    }

    private void JoinEffects(Projectile projectile)
    {
        var effectsToAdd = new List<EffectComponent>();

        foreach (var effect in projectile.Effects)
        {
            var oldEffect = _currentEffects.Find(p => p.GetType() == effect.GetType());
            if (oldEffect != null)
            {
                oldEffect.Join(effect);
            }
            else
            {
                effectsToAdd.Add(effect);
            }
        }

        _currentEffects.AddRange(effectsToAdd);

    }

    private void UpdateTurretColor()
    {
        _targetColor = Color.Lerp(Color.white, Color.red, (_power - MinPower) / MaxPower);
        renderer.material.color = Color.Lerp(renderer.material.color, _targetColor, Time.deltaTime * 5f);

    }


    protected Projectile CreateProjectile()
    {
        var o = Instantiate(Projectile) as GameObject;

        var projectile = o.GetComponent<Projectile>();

        projectile.Power = _power;

        projectile.Creator = gameObject;

        projectile.Effects = _currentEffects;

        projectile.transform.position = transform.position;

        renderer.material.color = Color.Lerp(Color.white, Color.red, (_power - MinPower) / MaxPower);
        return projectile;
    }
}
