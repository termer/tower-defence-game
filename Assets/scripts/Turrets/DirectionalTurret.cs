﻿using UnityEngine;
using System.Collections;

public class DirectionalTurret : Turret
{
    public Vector2 Direction = Vector2.right;

    public GameObject RotationSprite;

    protected override void InternalUpdate()
    {
        RotationSprite.transform.SetLocalEulerY(Mathf.Atan2(-Direction.y, Direction.x)*Mathf.Rad2Deg);

    }

    protected override void Shoot()
    {
        var projectile = CreateProjectile();
        projectile.Direction = Direction.normalized;
        projectile.transform.position = transform.position + (Vector3)Direction*0.32f;

        base.Shoot();
    }
}
