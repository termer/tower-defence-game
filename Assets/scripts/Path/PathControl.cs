﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Collections;

public class PathControl : MonoBehaviour
{
    public List<PathPoint> PathPoints; 
	// Use this for initialization
	void Start () 
    {
	
	}
	
	// UpdateMovement is called once per frame
	void Update () {
	
	}
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        for (int i = 0; i < PathPoints.Count - 1; i++)
        {
            Gizmos.DrawLine(PathPoints[i].transform.position, PathPoints[i + 1].transform.position);
        }

    }
}
