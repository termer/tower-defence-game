﻿using System;
using DN;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

	public enum CellType:byte
	{
		Free = 0,
		Wall = 1,
        WalkingCell = 2,
        Start = 3,
        End = 4
	}

public class TileMap : MonoBehaviour
{
    public struct Cell
    {
        public CellType Type { get; set; }
        public GameObject TileObject;
        public GameObject Turret;


        //Color(255f/255f, 153f/255f, 51f/255f, 255f/255f) : new Color(255f/255f, 255f/255f, 255f/255f, 255f/255f)

        public bool ShouldSetColor()
        {
            switch (Type)
            {
                case CellType.Free:
                    return false;
                    break;
                case CellType.Wall:
                    return false;
                    break;
                case CellType.WalkingCell:
                    return true;
                    break;
                case CellType.Start:
                    return true;

                    break;
                case CellType.End:
                    return true;
                    break;
                default:
                    return false;
            }
        }

        public Color GetColor()
        {
            switch (Type)
            {
                case CellType.Free:
                    return new Color(255f/255f, 255f/255f, 255f/255f, 255f/255f);
                    break;
                case CellType.Wall:
                    return new Color(255f/255f, 255f/255f, 255f/255f, 255f/255f);
                    break;
                case CellType.WalkingCell:
                    return new Color(255f/255f, 153f/255f, 51f/255f, 255f/255f);
                    break;
                case CellType.Start:
                    return new Color(255f / 255f, 0f / 255f, 51f / 255f, 255f / 255f);
                    break;
                case CellType.End:
                    return new Color(0f / 255f, 153f / 255f, 51f / 255f, 255f / 255f);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool IsLightActive()
        {
            return Type == CellType.WalkingCell 
                || Type == CellType.End 
                || Type == CellType.Start;
        }
    }

    public Rect Bounds { get; private set; }

    private Cell[,] _map;

    public Cell this[int i, int j]
    {
        get { return _map[i, j]; }
        set { _map[i, j] = value; }
    }

    public Cell this[Point p]
    {
        get { return _map[p.X, p.Y]; }
        set { _map[p.X, p.Y] = value; }
    }

    public int Width;
    public int Height;

    public float TileSize;
    public EventHandler MapChanged;

    public void Start()
    {
        _map = new Cell[Width, Height];

        Bounds = new Rect(0, 0, Width*64, Height*64);
    }

    public void TriggerTileMapChanged()
    {
        if (MapChanged != null)
        {
            MapChanged(this, EventArgs.Empty);
        }
    }

    public void FillWith(CellType cellType)
    {
        FillWith(Width, Height, cellType);
    }

    public void FillWith(int width, int height, CellType cellType)
    {
        for (var i = 0; i < width; i++)
            for (var j = 0; j < height; j++)
                _map[i, j] = new Cell {Type = cellType};
    }

    public void GenerateGameObjects(GameObject tile, GameObject wall)
    {
        for (var i = 0; i < Width; i++)
            for (var j = 0; j < Height; j++)
            {
                Cell cell = _map[i, j];
                cell.TileObject = tile.Create();

                TileData tileData = cell.TileObject.GetComponent<TileData>();
                tileData.SetLightActive(cell.IsLightActive());
                if (cell.ShouldSetColor())
                {
                    tileData.SetColor(cell.GetColor());
                }

                cell.TileObject.transform.position = new Vector3(i - Width/2f + transform.position.x + 0.5f, 0,
                    j - Height/2f + transform.position.z + 0.5f);

                cell.TileObject.transform.parent = transform;

                if (cell.Type == CellType.Wall)
                {
                    cell.Turret = wall.Create();
                    cell.Turret.transform.position = new Vector3(i - Width / 2f + transform.position.x + 0.5f, 0,
                        j - Height / 2f + transform.position.z + 0.5f);
                }

                _map[i, j] = cell;
            }
    }

    public Rect GetRect(int x, int y)
    {
        return new Rect(x*64.0f, y*64.0f, 64.0f, 64.0f);
    }

    public Rect GetRect(Point pos)
    {
        return new Rect(pos.Y*64.0f, pos.Y*64.0f, 64.0f, 64.0f);
    }

    public bool IsFree(Point p)
    {
        return _map[p.X, p.Y].Type == CellType.Free;
    }

    public bool IsFree(int x, int y)
    {
        return _map[x, y].Type == CellType.Free;
    }

    public bool IsWall(int x, int y)
    {
        return _map[x, y].Type == CellType.Wall;
    }

    public bool IsSolid(int x, int y)
    {
        return _map[x, y].Type == CellType.Wall;
    }

    public bool IsEndOfLadder(int x, int y)
    {
        return _map[x, y - 1].Type == CellType.Free;
    }

    public Point GetFinishCell()
    {
        for (int i = Height - 1; i >= 0; i--)
            for (int j = 1; j < Width; j++)
                if (_map[j, i].Type == CellType.End)
                {
                    return new Point(j, i);
                }
        throw new Exception("No end cell");
    }

    public Vector3 ToWorld(Point cell)
    {
        Vector3 worldTilePosition = GetTopLeftPos() + new Vector3(cell.X * TileSize + TileSize * 0.5f,
                                                                  0.5f,
                                                                  cell.Y * TileSize + TileSize * 0.5f);

        return worldTilePosition;
    }

    public Point ToTileMap(Vector3 positionWorld)
    {
        Vector3 positionInTileMap = positionWorld - GetTopLeftPos();
        Point tile = new Point(Mathf.RoundToInt((positionInTileMap.x - TileSize * 0.5f) / TileSize),
                               Mathf.RoundToInt((positionInTileMap.z - TileSize * 0.5f) / TileSize));
        return tile;
    }

    public Point GetStartCell()
    {
        for (int i = Height - 1; i >= 0; i--)
            for (int j = 1; j < Width; j++)
                if (_map[j, i].Type == CellType.Start)
                {
                    return new Point(j, i);
                }
        throw new Exception("No end cell");
    }

    public Point GetBottomFreeCell()
    {
        for (int i = Height - 1; i >= 0; i--)
            for (int j = 1; j < Width; j++)
                if (_map[j, i].Type == CellType.Free)
                    return new Point(j, i);
        return Point.Empty;
    }

    public Point GetTopFreeCell()
    {
        for (int i = 1; i < Height; i++)
            for (int j = Width - 1; j >= 0; j--)
                if (_map[j, i].Type == CellType.Free)
                    return new Point(j, i);
        return Point.Empty;
    }

    public Point GetLeftCell(int y, CellType cellType)
    {
        for (int i = 0; i < Width; i++)
            if (_map[i, y].Type == cellType)
                return new Point(i, y);
        return Point.Empty;
    }

    public Point GetRightCell(int y, CellType cellType)
    {
        for (int i = Width - 1; i >= 0; i--)
            if (_map[i, y].Type == cellType)
                return new Point(i, y);
        return Point.Empty;
    }

    public Point GetRightCell(CellType cellType)
    {
        Point max = Point.Empty;
        for (int i = 0; i < Height; i++)
        {
            Point p = GetRightCell(i, cellType);
            if (p.X > max.X)
                max = p;
        }
        return max;
    }


    /// <summary>
    /// may be not empty
    /// </summary>
    public Point GetRandomPoint()
    {
        return new Point(RandomTool.NextInt(0, Width), RandomTool.NextInt(0, Height));
    }

    public Point GetRandomEmptyPoint()
    {
        Point p;
        do
        {
            p = new Point(RandomTool.NextInt(0, Width), RandomTool.NextInt(0, Height));
        } while (!IsFree(p));

        return p;
    }

    public Point GetRandomStandablePoint()
    {
        Point p;
        do
        {
            p = new Point(RandomTool.NextInt(0, Width), RandomTool.NextInt(0, Height - 1));
        } while (!IsFree(p) || _map[p.X, p.Y + 1].Type != CellType.Wall);

        return p;
    }

    public bool InRange(Point cell)
    {
        return InRange(cell.X, cell.Y);
    }

    public bool InRange(int x, int y)
    {
        return x > 0 && x < Width && y > 0 && y < Height;
    }

    public Vector3 GetTopLeftPos()
    {
        return transform.position - new Vector3(Width*TileSize, 0, Height*TileSize)*0.5f;
    }

    public void LoadFromFile(string name)
    {
        try
        {
            CreateNewMapFromFile(name);
        }
        catch (Exception exception)
        {
            Debug.Log(exception.ToString());
        }
    }

    private void CreateNewMapFromFile(string name)
    {
        var lines = ReadRawLevelFromFile(name);
        CreateNewMap(lines);
        ReadMapFromString(lines);
    }

    private static string[] ReadRawLevelFromFile(string name)
    {
        string path = GetPathToLevel(name);
        string[] lines = File.ReadAllLines(path);
        return lines;
    }

    private static string GetPathToLevel(string name)
    {
        return LevelAssetPathHelper.GetRootFolder() + name + ".level";
    }

    private void CreateNewMap(string[] lines)
    {
        Width = lines.Length;
        Height = lines[0].Length;
        _map = new Cell[Width, Height];
    }

    private void ReadMapFromString(string[] lines)
    {
        for (int j = 0; j < Height; j++)
        {
            for (int i = 0; i < Width; i++)
            {
                _map[i, j].Type = (CellType) char.GetNumericValue(lines[i][j]);
            }
        }
    }

    public void PrintDebug()
    {
        try
        {
            TextWriter w = System.IO.File.CreateText(@"E:\map_debug.txt");
            for (int j = 0; j < Height; j++)
            {
                w.WriteLine();
                for (int i = 0; i < Width; i++)
                {
                    w.Write(((byte)_map[i, j].Type).ToString());
                }
            }
            w.Close();
        }
        catch (Exception exception)
        {
            Debug.Log(exception.ToString());
        }
    }

    private void OnDrawGizmosSelected()
    {
        Color color = Color.blue;
        color.a = 0.3f;
        Gizmos.color = color;

        Gizmos.DrawCube(transform.position, new Vector3(Width * TileSize, 1, Height * TileSize));
        Gizmos.color = Color.red;

        Gizmos.DrawCube(GetTopLeftPos(), new Vector3(1,1,1)*0.1f);
    }
}