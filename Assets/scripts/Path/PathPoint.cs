﻿using UnityEngine;
using System.Collections;

public class PathPoint : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// UpdateMovement is called once per frame
	void Update () {
	
	}

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.62f);
    }
}
